# Getting Started

### Reference Documentation

* [Analysis documentation](https://drive.google.com/file/d/15dfmJXI8Vwsg7adtkqNACgJhi4oh8vam/view?usp=sharing)
* [Javadoc](src/main/resources/static/javadoc/index.html)
* [Amazon API Documentation](amazonEndpoint.md)
* [Assigment documentation](https://drive.google.com/file/d/1Rny59onjyA3P17dYe_2oHkWmX-6eAev0/view?usp=sharing)

## Running with maven ##

### Requirements ###

* Java 8
* Maven

Go to the project root path and run:

```
mvn clean compile
mvn spring-boot:run
```

## Running with docker ##

### Requirements ###

* Docker

Go to the project root path and run:

```
docker-compose up
```

*The project image is already on Docker hub but, In case you prefer to build it by yourself, run the command below:*

```
docker build . -t mairelinrr/sellic-score-challenge
```

