#Stage One
FROM maven:3.5.4-jdk-8-alpine as mavenimg
COPY ./pom.xml ./pom.xml
COPY ./src ./src
RUN mvn dependency:go-offline -B
RUN mvn package

#Stage Two
FROM openjdk:8-jdk-alpine

WORKDIR /opt/app

COPY --from=mavenimg target/*.jar /opt/app/scorechallenge.jar

ENTRYPOINT ["java","-jar","scorechallenge.jar","--server.port=8080"]

#docker build . -t mairelinrr/sellic-score-challenge
