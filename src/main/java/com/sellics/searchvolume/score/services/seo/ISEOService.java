package com.sellics.searchvolume.score.services.seo;


import javax.xml.ws.http.HTTPException;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author Mairelin
 * Interface to handle the suggestion list.
 */
public interface ISEOService<T> {

    /**
     * @author Mairelin
     * @param keyWord would to research
     * @param params list of query params to tbe attached to the autocomplete API request.
     * @return List of suggestions returned by the consumed autocomplete API.
     * @throws IOException by the process that reads the response stream
     * @throws HTTPException when the amazon aPI return a HTTP code different from 200
     */
    List<T> getSuggestions(String keyWord, Map<String, String> params) throws IOException, HTTPException;
}
